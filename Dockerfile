FROM node:11.10.0-slim
RUN mkdir ./app
RUN chmod 777 ./app
WORKDIR /app
ENV TZ=Asia/Kolkata
RUN echo "deb http://archive.debian.org/debian stretch main" > /etc/apt/sources.list
RUN apt update && apt -y install python build-essential
WORKDIR /app
COPY package.json package-lock.json ./
RUN npm install
RUN uname -a
COPY . .
RUN chmod +x docker-entrypoint.sh
ENTRYPOINT ["./docker-entrypoint.sh"]
CMD ["node", "server"]
